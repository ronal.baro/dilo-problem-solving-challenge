﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScOther8 : MonoBehaviour
{
    [SerializeField] private int _point;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            ScScore7._instance.AddScore(_point);
            Recycle();
        }
    }

    private void Recycle()
    {
        ScLevelManager8.callEvent.SpawnEvent(1, 3);
        this.gameObject.SetActive(false);
    }

    //deactiveforever()...





}
