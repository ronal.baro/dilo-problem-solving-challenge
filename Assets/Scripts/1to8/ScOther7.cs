﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScOther7 : MonoBehaviour
{
    [SerializeField] private int _point;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            this.gameObject.SetActive(false);
           
            ScScore7._instance.AddScore(_point);
        }
    }


}
