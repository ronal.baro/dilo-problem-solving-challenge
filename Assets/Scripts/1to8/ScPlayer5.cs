﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScPlayer5 : MonoBehaviour
{
    Rigidbody2D _rb;
    private Vector2 _targetPos;
    public float _moveSpeed;
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        _targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    
    }
    void FixedUpdate()
    {
    //_rb.transform.position = Vector2.MoveTowards(transform.position, _targetPos, _moveSpeed *Time.fixedDeltaTime);
    //jittering

    _rb.MovePosition (Vector2.MoveTowards(transform.position, _targetPos, _moveSpeed *Time.fixedDeltaTime));
    }
}
