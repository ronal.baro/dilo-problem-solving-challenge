﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScPlayer8 : MonoBehaviour
{
    Rigidbody2D _rb;
    float _x;
    float _y;
    private Vector2 _dir;
    public float _moveSpeed;
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        ScLevelManager8.callEvent.onHold += MoveToTarget;
    }
    void Update()
    {
        _x = Input.GetAxisRaw("Horizontal");
        _y = Input.GetAxisRaw("Vertical");
        _dir.x = _x;
        _dir.y = _y;
    }
    void FixedUpdate()
    {
        _rb.velocity = _dir * _moveSpeed;
    }

    private void MoveToTarget(Vector2 pos)
    {
        _rb.MovePosition(Vector2.MoveTowards(transform.position, pos, _moveSpeed * Time.fixedDeltaTime));

    }
}
