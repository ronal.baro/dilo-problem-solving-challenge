﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ScLevelManager3 : MonoBehaviour
{
    [Header ("Walls")]
    public Camera _camera;
    private float _lastRatio;
    Vector2 _min;
    Vector2 _max;
    public GameObject _prefabWall;
    GameObject[] _walls = new GameObject[4];


    void Start()
    {
        SetWall();
    }

    void Update()
    {
        if(_lastRatio != _camera.aspect){
            DeleteWall();
            SetWall();
        }
    }

    void SetWall()
    {   
        float halfHeight = _camera.orthographicSize;
        float halfWidth = _camera.aspect * halfHeight;
        
        _lastRatio = _camera.aspect;

        _min.Set(halfWidth, -halfHeight);
        _max.Set(-halfWidth, halfHeight);

        _walls[0] = Instantiate(_prefabWall, Vector3.up * halfHeight, Quaternion.identity);
        _walls[0].transform.localScale = Vector3.one / 2 + Vector3.left * (halfWidth * 2);
        _walls[1] = Instantiate(_walls[0], Vector3.down * halfHeight, Quaternion.identity);

        _walls[2] = Instantiate(_prefabWall, Vector3.right * halfWidth, Quaternion.identity);
        _walls[2].transform.localScale = Vector3.one / 2 + Vector3.up * (halfHeight * 2);
        _walls[3] = Instantiate(_walls[2], Vector3.left * halfWidth, Quaternion.identity);

        for (int i = 0; i < _walls.Length; i++)
        {
            _walls[i].transform.SetParent(transform);
        }
        //pake canvas aja mending lol..
    }

    void DeleteWall(){
        for (int i = 0; i < _walls.Length; i++)
        {
            Destroy(_walls[i]);
        }
        System.Array.Clear(_walls,0,_walls.Length);
    }
}
