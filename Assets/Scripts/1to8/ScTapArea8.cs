﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class ScTapArea8 : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    Vector2 _pos;
    bool hold;
    public void OnPointerDown(PointerEventData eventData)
    {
        hold = true;
    }


    public void OnPointerUp(PointerEventData eventData)
    {
        hold = false;
    }

    private void FixedUpdate()
    {

        if (hold)
        {
            _pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //ScLevelManager6.callEvent.onHold(_pos);
             ScLevelManager8.callEvent.HoldEvent(_pos);
          
        }
    }

}
