﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ScLevelManager6 : MonoBehaviour

{
    public static ScLevelManager6 callEvent = null;

    [Header("Walls")]
    public Camera _camera;//camera static rupanya
    private float _lastRatio;
    Vector2 _min;
    Vector2 _max;
    float halfHeight;
    float halfWidth;
    public GameObject _prefabWall;
    GameObject[] _walls = new GameObject[4];

    [Header("The Others")]
    public ScOther6 _prefabOther;
    bool _isSpawning;
    public int _theOthersCount;
    public float _spawnDistance;
    private List<ScOther6> _theOthers;

    public Dictionary<string, Vector2> _direction = new Dictionary<string, Vector2>(9){
        {"North",Vector2.up},
        {"South",Vector2.down},
        {"West",Vector2.left},
        {"East",Vector2.right},
        {"NorthWest", new Vector2(-1,1)},
        {"SouthEast", new Vector2(1,-1)},
        {"NorthEast", Vector2.one},
        {"SouthWest", -Vector2.one}
    };

    //event
    //public delegate void holdHandler(Vector2 a);
    //public static event holdHandler onHold;

    //public UnityEngine.Events.UnityAction<Vector2> onHold;
    //public event System.Action<Vector2> onHold; //hanya bisa dipanggil dari dalam kecuali subscribe
    public System.Action<Vector2> onHold;


    private void Awake()
    {
        callEvent = this;
        _theOthers = new List<ScOther6>();
    }
    void Start()
    {
        SetWall();
        StartCoroutine(SpawnOthers(_theOthersCount));
    }

    void Update()
    {
        if (_lastRatio != _camera.aspect)
        {
            DeleteWall();
            SetWall();
        }
    }



    //Wall for Live
    void SetWall()
    {
        halfHeight = _camera.orthographicSize;
        halfWidth = _camera.aspect * halfHeight;

        _lastRatio = _camera.aspect;

        _min.Set(halfWidth, -halfHeight);
        _max.Set(-halfWidth, halfHeight);

        _walls[0] = Instantiate(_prefabWall, Vector3.up * halfHeight, Quaternion.identity);
        _walls[0].transform.localScale = Vector3.one / 2 + Vector3.left * (halfWidth * 2);
        _walls[1] = Instantiate(_walls[0], Vector3.down * halfHeight, Quaternion.identity);

        _walls[2] = Instantiate(_prefabWall, Vector3.right * halfWidth, Quaternion.identity);
        _walls[2].transform.localScale = Vector3.one / 2 + Vector3.up * (halfHeight * 2);
        _walls[3] = Instantiate(_walls[2], Vector3.left * halfWidth, Quaternion.identity);

        for (int i = 0; i < _walls.Length; i++)
        {
            _walls[i].transform.SetParent(transform);
        }
        //pake canvas aja mending lol..
    }

    void DeleteWall()
    {
        for (int i = 0; i < _walls.Length; i++)
        {
            Destroy(_walls[i]);
        }
        System.Array.Clear(_walls, 0, _walls.Length);
    }

    //Controll
    public void HoldEvent(Vector2 pos)
    {
        if (onHold != null)
        {
            onHold(pos);
        }
    }

    //Spawn

    public Vector2 GetRandomPosition()
    {
        Vector2 pos;
        pos.x = Random.Range(_min.x, _max.x);
        pos.y = Random.Range(_min.y, _max.y);
        return pos;
    }

    private IEnumerator SpawnOthers(int count)
    {
        for (int i = 0; i < count; i++)
        {
            SpawnOther();

            yield return new WaitUntil(() => { return !_isSpawning; });
        }
    }
    public void SpawnOther()
    {
        _isSpawning = true;
        Vector2 pos = GetRandomPosition();
        StartCoroutine(CheckPos(pos, (Vector2 newPos, bool isSpawn) => { if (!isSpawn) { _theOthers.Add(Instantiate(_prefabOther, newPos, Quaternion.identity)); } }));

    }

    private IEnumerator CheckPos(Vector2 pos, System.Action<Vector2, bool> onCompleted)
    {
        Collider2D hit;
        int i = 0;
        bool found;
        do
        {
            i++;
            Debug.Log(i);
            found = false;

            hit = Physics2D.OverlapCircle(pos, _spawnDistance);
            //Debug.Log(hit.Length);
            Color col;
            switch (i)
            {
                case 1: col = Color.red; break;
                case 2: col = Color.yellow; break;
                case 3: col = Color.green; break;
                default: col = Color.white; break;
            }
            Debug.DrawLine(pos + _direction["South"].normalized * _spawnDistance, pos + _direction["North"].normalized * _spawnDistance, col, 5);
            Debug.DrawLine(pos + _direction["West"].normalized * _spawnDistance, pos + _direction["East"].normalized * _spawnDistance, col, 5);
            Debug.DrawLine(pos + _direction["NorthWest"].normalized * _spawnDistance, pos + _direction["SouthEast"].normalized * _spawnDistance, col, 5);
            Debug.DrawLine(pos + _direction["NorthEast"].normalized * _spawnDistance, pos + _direction["SouthWest"].normalized * _spawnDistance, col, 5);

            if (hit)
            {
                pos = GetRandomPosition();
                found = true;
            }

        } while (found && i != 10);

        yield return null;
        onCompleted(pos, found);
        _isSpawning = false;
    }

    // private IEnumerator CheckPos(Vector2 pos, System.Action<Vector2, bool> onCompleted)
    // {
    //     Vector2 newPos = pos;
    //     RaycastHit2D hit;
    //     int i = 0;
    //     bool found;
    //     do
    //     {
    //         i++;
    //         Debug.Log(i);
    //         found = false;
    //         foreach (KeyValuePair<string, Vector2> dir in _direction)
    //         {
    //             hit = Physics2D.Raycast(newPos, dir.Value.normalized, 1);

    //             switch (i)
    //             {
    //                 case 1: Debug.DrawLine(newPos, newPos + dir.Value.normalized, Color.red, 5); break;
    //                 case 2: Debug.DrawLine(newPos, newPos + dir.Value.normalized, Color.yellow, 5); break;
    //                 default: Debug.DrawLine(newPos, newPos + dir.Value.normalized, Color.white, 5); break;
    //             }

    //             if (hit)
    //             {
    //                 newPos = GetRandomPosition();
    //                 found = true;

    //                 break;
    //             }
    //         }
    //     } while (found && i != 10);

    //     _isSpawning = false;
    //     yield return null;

    //     onCompleted(newPos, found);
    // }


}
