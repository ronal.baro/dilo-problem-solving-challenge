﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScScore7 : MonoBehaviour
{
    public static ScScore7 _instance = null;
    public Text ScoreText;

    double _score = 0;



    public double Score
    {
        get
        {
            return _score;
        }
        set
        {
            _score = value;
        }
    }

    private void Awake()
    {
        _instance = this;
    }

    public void AddScore(int point)
    {
        Score += point;
        ScoreText.text = "Score : " + Score.ToString();
    }

}
