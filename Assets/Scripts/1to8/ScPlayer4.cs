﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScPlayer4 : MonoBehaviour
{
    Rigidbody2D _rb;
    float _x;
    float _y;
    private Vector2 _dir;
    public float _moveSpeed;
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        _x = Input.GetAxisRaw("Horizontal");
        _y = Input.GetAxisRaw("Vertical");
        _dir.x = _x;
        _dir.y = _y;
        
    }
    void FixedUpdate()
    {
        _rb.velocity = _dir * _moveSpeed;
    }
}
