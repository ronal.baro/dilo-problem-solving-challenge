﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScNavMenu : MonoBehaviour
{
    public static ScNavMenu instance;
    private static float doubleTap = 2;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

    }
    public void GoToScene(int n)
    {
        doubleTap = 2;
        SceneManager.LoadScene(n, LoadSceneMode.Single);
       
    }
    private void Update()
    {
        doubleTap += Time.deltaTime;
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if ((doubleTap < 1))
            {
                if ( SceneManager.GetActiveScene().name == "Problem9")
                {
                     GoToScene(9);
                }
                else if ( SceneManager.GetActiveScene().buildIndex == 0)
                {
                    Application.Quit();
                }else
                {
                    GoToScene(0);
                }
            }
            doubleTap = 0; //firsttap
        }
    }

}
