﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScHealthEnemy : MonoBehaviour
{
    [SerializeField] private int _maxHP;//50

    [SerializeField] private int _point;//def 125
    private int EnemyId;
    public static int enemyCount = 0;
    public static int activeEnemy  = 0;
    
    public ScHealthEnemy()
    {
        enemyCount++;
        EnemyId = enemyCount;
        _point = (int)(_point * (enemyCount + 1) / 1.5f);//karna pake pooling gak ningkat kayaknya tapi  gaapalah anggap aja limit;
    }
    private int _HP;
    public int HP
    {
        get { return _HP; }
        set { _HP = value; }
    }

    private Animator _anim;

    public bool isDead;

    private void Awake()
    {
        _anim = GetComponentInChildren<Animator>();
    }
    private void Start()
    {   
        isDead = false;

        HP = _maxHP;
    }
    private void OnEnable()
    {
        activeEnemy++;
        HP = _maxHP;
    }

    private void OnDestroy()
    {
        CancelInvoke();
    }
    private void OnDisable()
    {
       
        isDead = false;
        CancelInvoke();
    }

    public void ReduceHP(int point)
    {
        if (isDead) return;

        HP -= point;
        ScLevelManager9.callEvent.AddScoreEvent(enemyCount*5);
        _anim.SetTrigger("Hurt");

        if (HP <= 0&&!IsInvoking("StatisticallyDead"))
        {     ScAudioManager.Play.EnemySFX(AudioTag.ENEMY_DEAD);
            isDead = true;
            Invoke("StatisticallyDead", 1f);
            _anim.SetTrigger("Dead");//animator buat masalah.. lain kali usahakan gak ubah value...
            //!PENTING.. terus simpan animator di gameobject parent, biar gak gila..
            isDead = true;
        }
    }

    public void OfficiallyDead()
    {   GetComponent<CapsuleCollider2D>().enabled = true;
        gameObject.SetActive(false);
        //Destroy(this);//?terpaksa aneh banget nilainya berubah gegara animator...
    }
    public void StatisticallyDead()
    {
        transform.Find("Sprite").gameObject.SetActive(false);
        ScLevelManager9.callEvent.AddScoreEvent(_point);
        Invoke("OfficiallyDead", 5f);
    }

   

}
