﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScTapGameOver : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    bool down = false;
    float count = 0;
    public void OnPointerDown(PointerEventData eventData)
    {
        count = 0;
        down = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        down = false;
        if (count < 0.5f)
        {
            ScLevelManager9.callEvent.restartStatic();
            UnityEngine.SceneManagement.SceneManager.LoadScene("Opening");
        }
    }

    private void Update()
    {
        if (down)
        {
            count += Time.deltaTime;
            if (count > 1.75)
            {
                ScLevelManager9.callEvent.restartStatic();
                UnityEngine.SceneManagement.SceneManager.LoadScene("Problem9");
            }
        }
    }

}
