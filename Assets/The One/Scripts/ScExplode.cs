﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScExplode : MonoBehaviour
{
    ParticleSystem fx;
    void Awake()
    {
        fx = GetComponent<ParticleSystem>();
    }

    private void OnEnable()
    {
        fx.Play();
    }
}


