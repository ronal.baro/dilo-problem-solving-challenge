﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScUIManager9 : MonoBehaviour
{   //Aku bingung mana yang perlu singleton 
    #region Single
    private static ScUIManager9 _instance = null;
    public static ScUIManager9 Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ScUIManager9>();
                if (_instance == null)
                {
                    Debug.Log("No Object ScUIManager9");
                }
            }
            return _instance;
        }
    }
    #endregion

    //VALUE
    private ScHealthPlayer Player;
    private ScScore9 Score;

    [Header("UI")]//UI
    public Text degree;
    public Slider HPBar;
    public Text HPText;
    public Image ImageBar;
    public Text ScoreText;
    public Image FlashingLight;
    public Image GameOverPanel;
    public CanvasGroup GamePlayUI;
    public CanvasGroup GameOverGroup;

    [Header("Value")]//AESTHETIC VALUE
    public Color LowHPColor;
    public Color NormalHPColor;
    private void Awake()
    {
        _instance = this;
    }
    void Start()
    {
        ScLevelManager9.callEvent.onGameOver += GameOverAnimation;
        Score = ScScore9.Instance;
        Player = FindObjectOfType<ScHealthPlayer>();

        HPBar.maxValue = Player.MaxEnergy;
        HPBar.value = Player.MaxEnergy;
    }
    //HP BAR
    public void SetCurrentHealth()
    {
        HPBar.value = Player.Energy;
        HPText.text = HPBar.value.ToString();
        if (HPBar.value <= 25)
        {
            if (!IsInvoking("LowColorAnimationPlay"))
                InvokeRepeating("LowColorAnimationPlay", 0, 0.25f);
        }
        else { CancelInvoke(); SetColorToNormal(); }


    }

    void SetColorToNormal()
    {
        ImageBar.color = NormalHPColor;
    }
    void SetColorToLow()
    {
        ImageBar.color = LowHPColor;

    }
    void LowColorAnimationPlay()
    {
        if (ImageBar.color == NormalHPColor)
        {
            SetColorToLow();
        }
        else
        {
            SetColorToNormal();
        }
    }
    void GameOverAnimation()
    {
        FlashingLight.gameObject.SetActive(true);
        ScoreText.gameObject.SetActive(false);
        HPText.gameObject.SetActive(false);
        HPBar.gameObject.SetActive(false);
        Invoke("ShowGameOverPanel", 3);
    }
    void ShowGameOverPanel()
    {
        GameOverPanel.gameObject.SetActive(true);
        FlashingLight.gameObject.SetActive(false);
        StartCoroutine(ShowRestartOption());
    }

    private IEnumerator ShowRestartOption()
    {
        while (GameOverGroup.alpha < 1)
        {
            GameOverGroup.alpha += 1f * Time.deltaTime;
            yield return null;
        }

        yield return new WaitForSeconds(1.5f);

        Transform text = GameOverGroup.transform.Find("Restart");
        Transform Go = GameOverGroup.transform.Find("GO");
        text.gameObject.SetActive(true);
        GameOverGroup.interactable = true;

        Vector2 target = text.localPosition;
        text.localPosition = new Vector2(-1500, text.localPosition.y);
        float count = 0;
        while (count < 1)
        {
            count += Time.deltaTime;
            text.localPosition = Vector2.MoveTowards(text.localPosition, target, 25 + count * Time.deltaTime);
            Go.localPosition = (Vector2)(Go.localPosition) + Vector2.up*60*Time.deltaTime;
            yield return null;
        }
        Text highscoreText = GameOverGroup.transform.Find("Highscore").GetComponent<Text>();
        if (Score.Score > Score._highscore)
        {
            Score._highscore = Score.Score;
            highscoreText.text = $"NEW HIGHSCORE\n<size=60>{Score._highscore}</size>";
        }
        else
        {
            highscoreText.text = $"HIGHSCORE\n<size=60>{Score._highscore}</size>";
        }
        highscoreText.gameObject.SetActive(true);



    }

    //SCORE TEXT
    public void SetScore()
    {
        ScoreText.text = Score.Score.ToString();
    }

    void Update()
    {
        degree.text = ScPlayer9.degree.ToString("0000");
    }

    private void OnDestroy()
    {
        CancelInvoke();
    }
}
