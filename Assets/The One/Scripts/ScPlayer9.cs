﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScPlayer9 : MonoBehaviour
{
    Rigidbody2D _rb;
    float _x;
    float _y;
    private Vector2 _dir;
    public float _moveSpeed;//0.5impulse, 
    public static float degree;
    bool _isMoving;
    bool _isHold;
    private Vector2 _relativeDir;
    private float _relativeDistance;
    public float _rotationSpeedBase;
    public Transform _eyeOuter;
    public Transform _eye;
    public ParticleSystem _sparkyFx;
    private Vector2 _eyeRelativePos;
    public ScEnergySpit spit;
    public GameObject spitBurst;
    
    public bool hasSpit;
    void Start()
    {   hasSpit = false;
        _rb = GetComponent<Rigidbody2D>();
        _isMoving = true;
        ScLevelManager9.callEvent.onHold += MoveToTarget;
        ScLevelManager9.callEvent.onGameOver += Dead;
        ScLevelManager9.callEvent.onAllowMove += AllowMove;
    }
    private void OnDestroy()
    {
        ScLevelManager9.callEvent.onHold -= MoveToTarget;
    }
    private void Dead()
    {
        Destroy(this.gameObject);
    }
    void Update()
    {
        _x = Input.GetAxisRaw("Horizontal");
        _y = Input.GetAxisRaw("Vertical");
        _dir.x = _x;
        _dir.y = _y;

        //tessPurpose
        Vector2 dirrect = (Vector2)transform.position - Vector2.one;
        degree = Vector2.SignedAngle(dirrect, Vector2.up);
        if (!spit.gameObject.activeSelf&&hasSpit&&(Input.GetMouseButtonDown(0)||Input.GetMouseButtonDown(1)||Input.GetMouseButtonDown(2)))
        {
            ScAudioManager.Play.PlayerSFX(AudioTag.ACID_BLAST,Random.Range(0.4f,0.6f));
            Shoot(_relativeDir);
            hasSpit = false;
            //ScUIManager9.spit;
        }

    }
    void FixedUpdate()
    {
        _eyeOuter.transform.Rotate(Vector3.forward * _rotationSpeedBase * Time.fixedDeltaTime);
        // _rb.velocity = _dir * _moveSpeed;
        if (_isMoving && _isHold)
        {   //if(!spit.gameObject.activeSelf)Shoot(_relativeDir);
            
            
            
            _relativeDistance = Vector2.SqrMagnitude(_relativeDir);
            if (_relativeDistance > 0.1f)
            {
               _rb.AddForce(_relativeDir *_moveSpeed, ForceMode2D.Force );//impulse 0.5 force 20
               // _rb.AddForce(_relativeDir.normalized * (_moveSpeed + Vector2.SqrMagnitude(_relativeDir)), ForceMode2D.Force );//100 hanya bisa force//terlalu mudah
                _eye.localPosition = Vector2.MoveTowards(_eye.localPosition, _relativeDir.normalized * 0.5f, Time.fixedDeltaTime );
                _eyeOuter.localPosition = Vector2.MoveTowards(_eyeOuter.localPosition, _relativeDir.normalized * 0.3f, Time.fixedDeltaTime * 0.6f);
                _eyeOuter.transform.Rotate(Vector3.forward * (_rotationSpeedBase + (5 * _relativeDistance)) * Time.fixedDeltaTime);
            
            }
        }
        else
        {
             if (_eye.localPosition == Vector3.zero) { return; }
            else
            {
                _eye.localPosition = Vector2.MoveTowards(_eye.localPosition, Vector2.zero, Time.fixedDeltaTime * 2);
                _eyeOuter.localPosition = Vector2.MoveTowards(_eyeOuter.localPosition, Vector2.zero, Time.fixedDeltaTime * 4);
            }

        }


    }
    void Shoot(Vector2 pos){
        Vector2 dir = new Vector2(-_relativeDir.x,_relativeDir.y).normalized;//? kenapa y nya terbalik kalau pake signed angle
        Quaternion rot = Quaternion.Euler(0, 0, Vector2.SignedAngle(dir, Vector2.up));
        
        spit.transform.position = transform.position;
        spit.transform.rotation = rot;
        spit.gameObject.SetActive(true);

        spitBurst.transform.rotation = rot;
        spitBurst.gameObject.SetActive(true);
        
        spit.GetComponent<Rigidbody2D>().AddForce(pos.normalized*15,ForceMode2D.Impulse);
        //GameObject a =Instantiate(spit,transform.position,rot);
       
        
    }
    private void MoveToTarget(bool isHold, Vector2 pos)
    {
        // _rb.MovePosition(Vector2.MoveTowards(transform.position, pos, _moveSpeed * Time.fixedDeltaTime));
        //if(Vector2.Distance(pos,_rb.transform.position)>0.1f)
        //_rb.transform.position = Vector2.MoveTowards(transform.position,pos, _moveSpeed *Time.fixedDeltaTime);

        _isHold = isHold;
        _relativeDir = pos - (Vector2)transform.position;//?gak tau kenapa gak di normalized lebih nyaman?

        //_eyeRelativePos;
        // _eye.localPosition = Random.insideUnitCircle *0.5f;
    }
    private void AllowMove(bool allow)
    {
        _isMoving = allow;
        _sparkyFx.Play();
        ScAudioManager.Play.PlayerSFX(AudioTag.SHOCK,1.5f);
    }
}
