﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScEnergyFuel : MonoBehaviour
{
    [SerializeField] private int _point;
    [SerializeField] private float _timeAlive; //default 3
    public float _mekarSize; //0.4;
    private float _countUp;


    public static bool isPlaying = true;
    private bool _mekar;


    private void Start()
    {
        _mekar = false;
        _countUp = -2;
        transform.localScale = Vector2.zero;

        ScLevelManager9.callEvent.onGameOver -= Deactive;
        ScLevelManager9.callEvent.onGameOver += Deactive;
    }
    private void OnDestroy()
    {
        ScLevelManager9.callEvent.onGameOver -= Deactive;

    }
    private void OnEnable()
    {
        if (isPlaying)
        {
            _mekar = false;
            _countUp = 0;
            InvokeRepeating("Reposition", 5, _timeAlive);
        }
        else Deactive();
    }
    private void OnDisable()
    {
        CancelInvoke();
    }


    private void Deactive()
    {
        if (isPlaying) { isPlaying = false; }
        this.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //pakeEventNanti
            if (!ScLevelManager9.callEvent._player.hasSpit)
            {
                ScAudioManager.Play.PlayerSFX(AudioTag.HAS_SPIT,Random.Range(1,1.5f));
                ScLevelManager9.callEvent._player.hasSpit = true;
            }else
            {
                ScAudioManager.Play.PlayerSFX(AudioTag.PICK_UP);
            }
         
            ScLevelManager9.callEvent.AddScoreEvent(_point);
            ScLevelManager9.callEvent.AddEnergyEvent(_point);
            Recycle();
        }
    }

    private void Recycle()
    {
        ScLevelManager9.callEvent.SpawnEvent(((byte)ScLevelManager9._theOthers.ITEM), 1, 3);
        gameObject.SetActive(false);

    }

    public void Reposition()
    {
        Vector2 pos = ScLevelManager9.callEvent.GetRandomPosition();
        StartCoroutine(ScLevelManager9.callEvent.CheckPos(pos));
        transform.position = pos;
        _mekar = false;
        _countUp = 0;
    }

    //deactiveforever()...
    private void Update()
    {
        _countUp += Time.deltaTime;
        if (!_mekar) //masuk/membesar
        {//+ (_countUp / _timeAlive) ini buat accelerasi
            transform.localScale = Vector2.Lerp(transform.localScale, Vector2.one * _mekarSize, (10 / 2 * _timeAlive) * Time.deltaTime);

            if (_countUp >= 0.2 * _timeAlive || transform.localScale.x >= _mekarSize)
            {
                _mekar = true;
                // Debug.Log("MEKRRRRR" + transform.localScale.x);
            }

        }
        else
        {
            if (_countUp < (0.7f * _timeAlive))
                return;

            transform.localScale = Vector2.Lerp(transform.localScale, Vector2.one * 0.05f, (0.65f * _timeAlive) * Time.deltaTime);
            //if (_countUp >= _timeAlive) { _mekar = false; _countUp = 0; }main aman aja


        }

    }

}





