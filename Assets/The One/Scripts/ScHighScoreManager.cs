﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ScHighScoreManager
{
    private const string HIGHSCORE = "Highscore";

    //[System.Serializable] nah class
    private static int highscore;
    public static int Highscore
    {
        get { return highscore; }
        set { highscore = value; }
    }

    public static void Load()
    {
        if (PlayerPrefs.HasKey(HIGHSCORE))
        {  
            //string json = PlayerPrefs.GetString(HIGHSCORE);
            //highscore = JsonUtility.FromJson<double>(json);coba2 aja dan gak bisa
            highscore = PlayerPrefs.GetInt(HIGHSCORE);

        }
        else
        {  
            highscore = 0;
            Save();
        }
    }

    public static void Save(){
      //string json = JsonUtility.ToJson(highscore);
        //PlayerPrefs.SetString(HIGHSCORE,json);
        
       
        PlayerPrefs.SetInt(HIGHSCORE,highscore);
    }


}


