﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScScore9 : MonoBehaviour
{
    private static ScScore9 _instance = null;
    public static ScScore9 Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ScScore9>();
                if (_instance == null)
                {
                    Debug.Log("No Object ScScore9");
                }
            }
            return _instance;
        }
    }

    int _score = 0;
    internal int _highscore;

    public int Score
    {
        get
        {
            return _score;
        }
        set
        {
            _score = value;
        }
    }

    private void Awake()
    {
        _instance = this;
    }

    public void AddScore(int point)
    {
        Score += point;
    }


}
