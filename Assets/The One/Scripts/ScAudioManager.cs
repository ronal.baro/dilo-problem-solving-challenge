﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioTag
{
    public const string PLAYER_SPAWN = "PlayerSpawn";
    public const string ENEMY_SPAWN = "EnemySpawn";
    public const string HAS_SPIT = "HasSpit";
    public const string PICK_UP = "PickUp";

    public const string SHOCK = "Shock";

    public const string ACID = "Acid";
    public const string BOOM_1 = "Boom1";

    public const string BOOM_2 = "Boom2";

    public const string WARP_OUT = "WarpOut";

    public const string CHARGING = "Charging";
    public const string PANIC_1 = "Panic1";
    public const string PANIC_2 = "Panic2";

    public const string ACID_BLAST = "AcidBlast";

    public const string IMPACT = "Impact";

    public const string ENEMY_DEAD = "EnemyDead";

    public const string MAINTHEME = "Main";

}
public class ScAudioManager : MonoBehaviour
{
    private static ScAudioManager _instance;
    public static ScAudioManager Play
    {
        get
        {
            return _instance;
        }
    }

    [SerializeField] AudioSource BGM;
    [SerializeField] AudioSource playerSFX;
    [SerializeField] AudioSource enemySFX;
    [SerializeField] AudioSource otherSFX;
    [SerializeField] AudioSource loopSFX;


    //1timeuse
    [System.Serializable]
    public struct AudioStruct
    {
        public string tag;
        public AudioClip file;
    }

    public AudioStruct[] _arrayStruct;
    //1timeuse

    private Dictionary<string, AudioClip> _audioDict = new Dictionary<string, AudioClip>();

    void Awake()
    {
        if (_instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        for (int i = 0; i < _arrayStruct.Length; i++)
        {
            _audioDict.Add(_arrayStruct[i].tag, _arrayStruct[i].file);
        }

    }
    public void PlayerSFX(string tag, float pitch = 1)
    {
        playerSFX.pitch = pitch;
        playerSFX.PlayOneShot(_audioDict[tag]);
    }
    public void EnemySFX(string tag, float pitch = 1)
    {
        enemySFX.pitch = pitch;
        enemySFX.PlayOneShot(_audioDict[tag]);
    }
    public void OtherSFX(string tag, float pitch = 1)
    {
        otherSFX.pitch = pitch;
        otherSFX.PlayOneShot(_audioDict[tag]);
    }
    public void BackGroundMusic(bool play)
    {
        if (play)
        {
            BGM.Play();
        }
        else
        {
            BGM.Stop();
        }
    }
    public void LoopSFX(string tag, float pitch = 1)
    {
        loopSFX.pitch = pitch;
        loopSFX.PlayOneShot(_audioDict[tag]);
    }

}
