﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ingat : MonoBehaviour
{


    #region cara 1
    public static class audioTag
    {
        public const string ALIEN = "alien";
        public const string BIRD = "bird";
        public const string DONG = "cupcup";

    }
    public List<AudioClip> listAudio;

    #endregion

    #region cara 2
    [System.Serializable]
    public struct AudioStruct
    {
        public string tag;
        public AudioClip file;
    }

    public List <AudioStruct> _listAudioStruct;

    #endregion

    public Dictionary<string, AudioClip> audioFile = new Dictionary<string, AudioClip>();
    private void Start()
    {//cara 1;
        audioFile.Add(audioTag.ALIEN, listAudio[0]);

        //cara 2;
        foreach (AudioStruct aStruct in _listAudioStruct){
            audioFile.Add(aStruct.tag,aStruct.file);
        }
    }
    public void Play()
    {
        string makan = audioTag.BIRD;
    }
}
