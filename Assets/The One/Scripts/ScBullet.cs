﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScBullet : MonoBehaviour
{
    public static bool isPlaying = true;
    Rigidbody2D _rb;

    public float _lockOnTargetDelay;

    public float _moveSpeed;

    private float countUp;

    private Vector3 _targetPos;
    private Vector2 _targetDir;
    [SerializeField] private int _point;
    public Transform rotator;
    bool _go;
    bool _friendlyFire;

    //tes

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }
    private void Start()
    {
        if (!isPlaying) { this.gameObject.SetActive(false); }
        ScLevelManager9.callEvent.onGameOver -= Deactive;
        ScLevelManager9.callEvent.onGameOver += Deactive;
        //ScLevelManager9.callEvent.onFriendlyFire += ActiveFriendlyFire;
    }
    private void OnDestroy()
    {
        ScLevelManager9.callEvent.onGameOver -= Deactive;
        ScLevelManager9.callEvent.onFriendlyFire -= ActiveFriendlyFire;
    }
    private void OnEnable()

    {
        _friendlyFire = false;
        _go = false;

        StartCoroutine(StartingPoint());

        //kedepannya coba kasih acceleration dan kasih radius jarak panjang lupa apa istilahnya

    }
    void Update()
    {
        if (countUp > _lockOnTargetDelay)
        {
            _targetPos = ScLevelManager9.callEvent._player.transform.position;
            _targetDir = (_targetPos - transform.position).normalized;
            countUp = 0;
        }
        else
        {
            countUp += Time.deltaTime;
        }


        //transform.position = Vector2.MoveTowards(transform.position, _targetPos, 5 * Time.deltaTime);


    }

    private void FixedUpdate()
    {
        if (_go)
        {
            _rb.AddForce(_targetDir.normalized * _moveSpeed);
            rotator.Rotate(-Vector3.forward * (90) * Time.fixedDeltaTime);
        }
    }

    IEnumerator StartingPoint()
    {
        _targetPos = ScLevelManager9.callEvent._player.transform.position;
        _targetDir = (_targetPos - transform.position).normalized;

        Debug.Log(_targetDir);
        _rb.AddForce(_targetDir.normalized * 10f, ForceMode2D.Impulse);

        yield return new WaitForSeconds(0.5f);
        _go = true;
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.tag == "Player")
        {
            ScAudioManager.Play.OtherSFX(AudioTag.BOOM_2, Random.Range(0.8f, 1.1f));
            other.gameObject.GetComponent<Rigidbody2D>().AddForce(_targetDir * 40, ForceMode2D.Impulse);

            Deactive2(other.gameObject.transform.position);
            ScLevelManager9.callEvent.ReduceEnergyEvent(_point);
        }
        else if (other.tag == "Spit" || other.tag == "Wall" || other.tag == "Bullet")
        {
            if (other.tag == "Spit")
            {
                float chance = Random.Range(0f, 2f);
                if (chance > 1f)
                {
                    return;
                }
            }
            ScAudioManager.Play.OtherSFX(AudioTag.BOOM_2, Random.Range(0.8f, 1.1f));

            RaycastHit2D hit = Physics2D.Linecast(transform.position, transform.position + Vector3.up, LayerMask.GetMask("Wall"));
            Debug.DrawLine(transform.position, transform.position + Vector3.up, Color.red, 7);

            Vector2 send;
            if (!hit)
            {
                Debug.DrawLine(transform.position, transform.position + Vector3.down, Color.yellow, 7);

                Physics2D.Raycast(transform.position, transform.position + Vector3.down, 1f, LayerMask.GetMask("Wall"));
                if (!hit)
                {
                    Debug.DrawLine(transform.position, transform.position + Vector3.right, Color.green, 7);
                    Physics2D.Raycast(transform.position, transform.position + Vector3.right, 1f, LayerMask.GetMask("Wall"));
                    if (!hit)
                    {
                        Debug.DrawLine(transform.position, transform.position + Vector3.down, Color.white, 7);

                        Physics2D.Raycast(transform.position, transform.position + Vector3.down, 1f, LayerMask.GetMask("Wall"));
                        if (!hit)
                        {
                            send = other.gameObject.transform.position;
                            Debug.Log("NOTHING");
                        }
                        else { send = hit.point; }
                    }
                    else { send = hit.point; }
                }
                else { send = -hit.point; }
            }
            else { send = -hit.point; Debug.Log("HERERERER"); }

            Deactive2(send);

        }
        else if (other.tag == "Enemy" && _friendlyFire)
        {
            ScAudioManager.Play.OtherSFX(AudioTag.BOOM_2, Random.Range(0.8f, 1.1f));

            other.gameObject.SetActive(false);
            Deactive2(other.gameObject.transform.position);
        }
    }

    private void Deactive()
    {
        if (isPlaying) { isPlaying = false; }
        this.gameObject.SetActive(false);
    }
    private void Deactive2(Vector2 pos)
    {
        Quaternion rot = Quaternion.Euler(0, 0, Vector2.SignedAngle(pos - (Vector2)transform.position, Vector2.up));

        ScLevelManager9.callEvent.PlayFX((byte)ScLevelManager9._theOthers.FXEXPLODE2, transform.position, rot);
        // fx.GetComponent<ScExplode>().Play();
        this.gameObject.SetActive(false);
    }

    private void ActiveFriendlyFire()
    {

    }



    //look up for target
}
