﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScEnemy : MonoBehaviour
{
    public static bool isPlaying = true;


    public float _attackRate; //7 default number
    public int _bulletPerAttack; //3
    public float _launchRate;    //1.2 

    private Vector3 _targetPos;
    public bool _allowMove = true;
    public float _moveSpeed;

    private bool _allowPower;
    [SerializeField] private int _damage;

    private Animator _anim;
    private Rigidbody2D _rb;
    private ScHealthEnemy _health;

    public Transform _sprite;
    private void OnEnable()
    {
        _sprite.gameObject.SetActive(true);
        _allowPower = false;
        _anim.SetTrigger("WarpIn");
        _allowMove = false;
        _health.isDead = false;
        _bulletPerAttack = Random.Range(_bulletPerAttack, _bulletPerAttack * 2);
        _launchRate = Random.Range(_launchRate - 0.2f, _launchRate + 0.2f);
        _attackRate = Random.Range(_attackRate, _attackRate + (_bulletPerAttack - 3));

        _targetPos = ScLevelManager9.callEvent.GetRandomPosition(2f);
        InvokeRepeating("Attack", 2, _attackRate); //animasi masuk sesuaikan dengan paramter time 
        ScAudioManager.Play.EnemySFX(AudioTag.ENEMY_SPAWN);
    }
    private void Awake()
    {
        _anim = GetComponentInChildren<Animator>();
        _rb = GetComponent<Rigidbody2D>();
        _health = GetComponent<ScHealthEnemy>();

    }
    void Start()
    {
        ScLevelManager9.callEvent.onGameOver -= Deactive;//biar cuma satu
        ScLevelManager9.callEvent.onGameOver += Deactive;//setelah beberapa hari baru sadar ini rupanya dipanggil semua object, jadi setiap kali event ini jalan ya, ada sebanyak jumlah object handlernya


        if (!IsInvoking("Attack"))
        {
            _targetPos = ScLevelManager9.callEvent.GetRandomPosition(2f);
            InvokeRepeating("Attack", 2, _attackRate);
        }
    }

    private void OnDestroy()
    {
        ScLevelManager9.callEvent.onGameOver -= Deactive;

    }
    private void OnDisable()
    {
        if (ScLevelManager9.callEvent.wasDestroyed) return;
        CancelInvoke();
        Debug.Log("NEXT ENEMY PLEASE");
        ScLevelManager9.callEvent.EnemyRespawnEvent();
        ScLevelManager9.callEvent.AllowMoveEvent(true);
    }

    private void AllowPower()
    {
        _allowPower = true;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_health.isDead) return;
        if (other.tag == "Enemy")
        {
            _targetPos = ScLevelManager9.callEvent.GetRandomPosition(2f);
        }
        if (other.tag == "Player" && !IsInvoking("ActiveShockwave"))
        {
            if (!_allowPower) return;
            ScLevelManager9.callEvent.AllowMoveEvent(false);
            ScAudioManager.Play.EnemySFX(AudioTag.CHARGING, 2f);
            GameObject fx = ScLevelManager9.callEvent.PlayFX((byte)ScLevelManager9._theOthers.FXRIPPLE2, transform.position, Quaternion.identity);
            fx.transform.SetParent(this.transform.Find("Sprite").transform);

            Invoke("ActiveShockwave", 0.5f);
        }
    }

    void ActiveShockwave()
    {      ScAudioManager.Play.EnemySFX("Impact",1.5f);
        ScLevelManager9.callEvent._player.gameObject.GetComponent<Rigidbody2D>().AddForce(
        (ScLevelManager9.callEvent._player.transform.position - transform.position).normalized
        * 100, ForceMode2D.Impulse);
        ScLevelManager9.callEvent.ReduceEnergyEvent(_damage);
        ScLevelManager9.callEvent.AllowMoveEvent(true);
    }

    void Attack()
    {
        if (_health.isDead) { CancelInvoke(); return; }
        _allowMove = true;
        _targetPos = _targetPos = ScLevelManager9.callEvent.GetRandomPosition(2f);
        if (ScLevelManager9.callEvent._player != null)
        {//1 biji delay  mengulang 1 detik dari invoke repeating
         //ScLevelManager9.callEvent.SpawnEvent(((byte)ScLevelManager9._theOthers.BULLET), 1, 0);

            //3 biji delay 1 detik...tidak mengulang
            ScLevelManager9.callEvent.SpawnEvent(((byte)ScLevelManager9._theOthers.BULLET), _bulletPerAttack, _launchRate, transform.position.x, transform.position.y, _sprite.gameObject);

        }
        //pos
        //addforce
    }

    void Wait()
    {
        _allowMove = true;
    }


    private void Deactive()
    {
        CancelInvoke();
        if (isPlaying && !_health.isDead) { isPlaying = false; }
        {
            _allowMove = false;
            _anim.SetTrigger("WarpOut");
            ScAudioManager.Play.EnemySFX(AudioTag.CHARGING, 1.25f);
            Invoke("PlayAudio", 1f);
            GameObject fx = ScLevelManager9.callEvent.PlayFX((byte)ScLevelManager9._theOthers.FXRIPPLE, transform.position, Quaternion.identity);
            fx.transform.SetParent(this.transform.Find("Sprite").transform);



            Invoke("Deactive2", 4f);
        }
    }
    void PlayAudio()
    {
        ScAudioManager.Play.OtherSFX(AudioTag.WARP_OUT, 1f);
    }


    void Deactive2()
    {
        this.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (_health.isDead) return;
        if (transform.position == _targetPos)
        {
            _targetPos = ScLevelManager9.callEvent.GetRandomPosition(1f);
        }
    }

    private void FixedUpdate()
    {

        if (_allowMove)
        {
            //transform.position = Vector2.MoveTowards(transform.position, _targetPos, _moveSpeed * Time.deltaTime);
            //_rb.position = Vector2.MoveTowards(_rb.transform.position, _targetPos, _moveSpeed * Time.deltaTime);
            _rb.position = Vector2.MoveTowards(_rb.transform.position, _targetPos, _moveSpeed * Time.fixedDeltaTime);

        }
    }
}