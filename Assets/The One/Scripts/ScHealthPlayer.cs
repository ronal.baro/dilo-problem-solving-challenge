﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScHealthPlayer : MonoBehaviour
{   
    [SerializeField] private int _maxEnergy;
    [SerializeField] private int _energyPerSec;
    private int _energy;
    public int Energy
    {
        get { return _energy; }
        set { _energy = value; }
    }
    public int MaxEnergy
    {
        get { return _energy; }
        set { _energy = value; }
    }

    private Animator _anim;
   
    private void Awake(){
        _anim = GetComponentInChildren<Animator>();
    }
    private void Start()
    {
        ScLevelManager9.callEvent.onAddEnergy += AddEnergy;
        ScLevelManager9.callEvent.onReduceEnergy += ReduceEnergy;
        
        Energy = _maxEnergy;
        //
      
        //
        InvokeRepeating("UseEnergy",0, 1);
    }

    private void OnDestroy()
    {
        ScLevelManager9.callEvent.onAddEnergy -= AddEnergy;
        ScLevelManager9.callEvent.onReduceEnergy -= ReduceEnergy;
        CancelInvoke();
    }
    private void AddEnergy(int point)
    {
        Energy += point;
        Energy = System.Math.Min(Energy, 100);
        _anim.SetInteger("Health",Energy);
        //test
        
    }
    private void ReduceEnergy(int point)
    {  
        Energy -= point;
        Energy = System.Math.Max(Energy, 0);
        //test
        _anim.SetInteger("Health",Energy);
        
        if (Energy <= 0&&!IsInvoking("GameOver"))
        {   ScAudioManager.Play.BackGroundMusic(false);
            ScAudioManager.Play.PlayerSFX(AudioTag.PANIC_2);
            _anim.SetTrigger("Dead");
           
            Invoke("GameOver",1.3f);
        }
            
          
    }

    private void UseEnergy()
    {   
       ScLevelManager9.callEvent.ReduceEnergyEvent(_energyPerSec);
    }

    public void GameOver()
    {  
       ScAudioManager.Play.PlayerSFX(AudioTag.BOOM_1,0.5f);
        ScLevelManager9.callEvent.PlayFX(((byte)ScLevelManager9._theOthers.FXEXPLODESHIP),transform.position,Quaternion.identity);
       
        ScLevelManager9.callEvent.GameOverEvent();
    }

}
