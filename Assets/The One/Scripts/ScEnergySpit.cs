using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScEnergySpit : MonoBehaviour
{
    Rigidbody2D _rb;

    [SerializeField] int _damage;
    bool isShrinking;
    private Transform _spitBurst;
    private Transform _spitTrail;
    Vector2 size = new Vector2(0.45f, 0.45f);//0.45 0.75
    Vector2 sizeTrail = new Vector2(0.75f, 0.75f);


    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _spitBurst = transform.Find("SpitFXBurst");
        _spitTrail = transform.Find("SpitTrail");
    }
    private void Start()
    {
        _spitBurst.gameObject.SetActive(false);
    }
    private void OnEnable()
    {

        isShrinking = false;
        Invoke("Disable", 3);//jaga2 andai keluar dinding
    }

    private void Disable()
    {   //cancel invoke disini agak bermasalah
        transform.localScale = size;
        _spitTrail.localScale = sizeTrail;
        _spitBurst.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
            ScAudioManager.Play.EnemySFX(AudioTag.ACID, 3f);

            if (!isShrinking)
            {
                GoShrink();
            }
        }


        if (other.tag == "Enemy")
        {
            ScAudioManager.Play.EnemySFX(AudioTag.ACID, Random.Range(1.5f, 2f));
            if (!isShrinking)
            {
                GoShrink();
            }
            // Random.Range(_damage,_damage+15)
            other.gameObject.GetComponent<ScHealthEnemy>().ReduceHP(Random.Range(_damage, _damage + 10));
        }
        else if (other.tag == "Wall")
        {
            ScAudioManager.Play.EnemySFX(AudioTag.ACID, 3f);

            if (!isShrinking)
            {
                GoShrink();
            }
            _rb.velocity = Vector2.zero;
        }

    }
    private void GoShrink()
    {
        CancelInvoke("Disable");
        _spitBurst.gameObject.SetActive(true);
        isShrinking = true;
        StartCoroutine(Shrinking(0.5f));
    }

    private IEnumerator Shrinking(float time)
    {
        float vThis = Vector2.Distance(transform.localScale, Vector2.zero) / time;
        float vSpit = Vector2.Distance(_spitTrail.localScale, Vector2.zero) / time;
        float count = time;
        while (count > 0)
        {
            count -= Time.deltaTime;
            transform.localScale = Vector2.MoveTowards(transform.localScale, Vector2.zero, vThis * Time.deltaTime);
            _spitTrail.localScale = Vector2.MoveTowards(_spitTrail.localScale, Vector2.zero, vSpit * Time.deltaTime);

            yield return null;
        }
        Disable();
    }
}
