﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScStar : MonoBehaviour
{   
    private void Awake() {
        SpriteRenderer r = GetComponent<SpriteRenderer>();
        transform.rotation = Quaternion.Euler(0,0,Random.Range(0,360));
        r.color = new Color(1,1,1, Random.Range(0.1f,0.8f)) ;  
    }
}
