﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScPlayerMenu : MonoBehaviour
{
    Rigidbody2D _rb;
    float _x;
    float _y;
    private Vector2 _dir;
    public float _moveSpeed;
    public static float degree;
    bool _isMoving;
    bool _isHold;
    private Vector2 _relativeDir;
    
    private Vector2 _randomDir;
    private float _relativeDistance;
    public float _rotationSpeedBase;
    public Transform _eyeOuter;
    public Transform _eye;
    private Vector2 _eyeRelativePos;

    void Start()
    {   InvokeRepeating("SetDir",2,3);
        _rb = GetComponent<Rigidbody2D>();
        _isMoving = true;
        ScMenuManager.Call.onHold += MoveToTarget;

        ScMenuManager.Call.onAllowMove += AllowMove;
    }
    
    void SetDir(){
        _randomDir = Vector2.zero + Random.insideUnitCircle *3;
    }
    private void Dead()
    {
        Destroy(this.gameObject);
    }
    void Update()
    {
        _x = Input.GetAxisRaw("Horizontal");
        _y = Input.GetAxisRaw("Vertical");
        _dir.x = _x;
        _dir.y = _y;

    }
    void FixedUpdate()
    {
       
        // _rb.velocity = _dir * _moveSpeed;
        if (_isMoving && _isHold)
        {
            _relativeDistance = Vector2.SqrMagnitude(_relativeDir);
            if (_relativeDistance > 0.1f)
            {
                _rb.AddForce(_relativeDir * _moveSpeed, ForceMode2D.Impulse);
                _eye.localPosition = Vector2.MoveTowards(_eye.localPosition, _relativeDir.normalized * 0.5f, Time.fixedDeltaTime);
                _eyeOuter.localPosition = Vector2.MoveTowards(_eyeOuter.localPosition, _relativeDir.normalized * 0.3f, Time.fixedDeltaTime * 0.6f);
                _eyeOuter.transform.Rotate(Vector3.forward * (_rotationSpeedBase + (5 * _relativeDistance)) * Time.fixedDeltaTime);

            }
        }
        else
        {    _rb.position = Vector2.MoveTowards(transform.position, _randomDir, (_moveSpeed + Vector2.Distance(_rb.position, _randomDir)) * Time.deltaTime); 
            _eyeOuter.transform.Rotate(Vector3.forward * _rotationSpeedBase * Time.fixedDeltaTime); 
            if (_eye.localPosition == Vector3.zero) { return; }
            else
            {
                _eye.localPosition = Vector2.MoveTowards(_eye.localPosition, Vector2.zero, Time.fixedDeltaTime * 1);
                _eyeOuter.localPosition = Vector2.MoveTowards(_eyeOuter.localPosition, Vector2.zero, Time.fixedDeltaTime * 2);
            }
            //_eye.localPosition = Vector2.MoveTowards(_eye.localPosition, _randomDir.normalized * 0.5f, Time.fixedDeltaTime);
            //_eyeOuter.localPosition = Vector2.MoveTowards(_eyeOuter.localPosition, _randomDir.normalized * 0.3f, Time.fixedDeltaTime * 0.6f);
            //_eyeOuter.transform.Rotate(Vector3.forward * (_rotationSpeedBase + (5 * Vector2.Distance(_rb.position, _randomDir))) * Time.fixedDeltaTime);

        }

    }
    

    private void MoveToTarget(bool isHold, Vector2 pos)
    {
        _isHold = isHold;
        _relativeDir = pos - (Vector2)transform.position;
    }
    private void AllowMove(bool allow)
    {
        _isMoving = allow;
    }
}
