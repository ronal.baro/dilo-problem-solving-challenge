﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ScMenuManager : MonoBehaviour
{
    private static ScMenuManager callEvent = null;
    public static ScMenuManager Call
    {
        get
        {
            if (callEvent == null)
            {
                callEvent = FindObjectOfType<ScMenuManager>();
            }
            return callEvent;
        }
    }

    public GameObject _prefabStar;
    public List<GameObject> Stars = new List<GameObject>();

    private Vector2 _min;
    private Vector2 _max;
    public float _spawnDistance;
    bool _isSpawning;

    public System.Action<bool, Vector2> onHold;
    public System.Action<bool> onAllowMove;

    public void HoldEvent(bool hold, Vector2 pos)
    {
        onHold?.Invoke(hold, pos);
    }
    public void AllowMoveEvent(bool Allow)
    {
        onAllowMove?.Invoke(Allow);
    }

    public void Play()
    {
        SceneManager.LoadScene("Problem9");
    }
    private void Start()
    {
        float halfHeight = Camera.main.orthographicSize;
        float halfWidth = Camera.main.aspect * halfHeight;

        _min.Set(halfWidth, -halfHeight);
        _max.Set(-halfWidth, halfHeight);

        StartCoroutine(SpawnMultipleObjects(25, _prefabStar));
        Invoke("DisableStarCollider", 1.35f);   //ini hanya iseng2 aja gak bakal begini kalau asli;
    }

    public Vector2 GetRandomPosition(float offset = 0.5f)
    {
        Vector2 pos;
        pos.x = Random.Range(_min.x - offset, _max.x + offset);
        pos.y = Random.Range(_min.y + offset, _max.y - offset);
        return pos;
    }
    private IEnumerator SpawnMultipleObjects(int count, GameObject Go)//tinggal tambah parameter sesuai dibutuhkan
    {
        Debug.Log(("Break"));
        for (int i = 0; i < count; i++)
        {
            _isSpawning = true;
            Vector2 pos = GetRandomPosition();
            pos = CheckPos(pos);//bila perlu check pos;
            //instantiate
            yield return new WaitUntil(() => _isSpawning == false);// !_isSpawning, biar gak terkecoh aja
            Stars.Add(Instantiate(Go, pos, Quaternion.identity));

        }
    }
    public Vector2 CheckPos(Vector2 pos)
    {
        bool found = false;
        Collider2D hit;
        int i = 0;

        do
        {
            i++;
            Debug.Log(i);
            found = false;

            hit = Physics2D.OverlapCircle(pos, _spawnDistance);
            if (hit)
            {
                pos = GetRandomPosition();
                found = true;
            }

        } while (found && i != 10);
        Debug.Log("Tidak kolisi"+ " pada iterasi ke- " + i);
        _isSpawning = false;
        return pos;
    }
    private void DisableStarCollider()
    {
        foreach (GameObject star in Stars)
        {
            CircleCollider2D col = star.gameObject.GetComponent<CircleCollider2D>();
            col.enabled = !col.enabled;
        }
    }
}
