﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class ScTapAreaMenu : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    Vector2 _pos;
    bool hold;
    public void OnPointerDown(PointerEventData eventData)
    {
        hold = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        hold = false;
    }

    private void Update()
    {
        _pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        ScMenuManager.Call.HoldEvent(hold, _pos);

    }

}
