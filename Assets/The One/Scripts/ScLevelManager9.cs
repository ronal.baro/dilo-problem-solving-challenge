﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ScLevelManager9 : MonoBehaviour

{   //suatu saat pisahkan event sama level biar pendek
    public static ScLevelManager9 callEvent = null;

    public bool wasDestroyed = false;

    internal ScScore9 Score;
    private ScUIManager9 UI;
    const int EnemyStartCount= 0;


    public ScPlayer9 _player;
    public ScHealthPlayer _playerHealth;

    private bool _gameOver;

    [Header("Walls")]
    public Camera _camera;//camera static rupanya
    private float _lastRatio;
    Vector2 _min;
    Vector2 _max;
    float halfHeight;
    float halfWidth;
    public GameObject _prefabWall;
    GameObject[] _walls = new GameObject[4];

    [Header("The Others")]
    public int _theOthersCount;
    public float _spawnDistance;
    //tes
    private int currentEnemyWave;
    private int activeEnemyCount;
    private List<int> waveInfo;

    // private List<ScEnergy> _energies = new List<ScEnergy>();

    public GameObject[] _theOthersPrefab;
    public enum _theOthers : byte { ITEM = 0, ENEMY = 1, BULLET = 2, FXEXPLODE2 = 3, FXEXPLODESHIP = 4, FXRIPPLE = 5, FXRIPPLE2 = 6, STAR = 7 };
    private Dictionary<byte, List<GameObject>> _AllOthers = new Dictionary<byte, List<GameObject>>(){
        {0, new List<GameObject>()},
        {1,new List<GameObject>()},
        {2,new List<GameObject>()},
        {3,new List<GameObject>()},
        {4,new List<GameObject>()},
        {5,new List<GameObject>()},
        {6,new List<GameObject>()},
        {7,new List<GameObject>()}
    };

    //spawn GameObjects
    bool _isSpawning;
    public Dictionary<string, Vector2> _direction = new Dictionary<string, Vector2>(9){
        {"North",Vector2.up},
        {"South",Vector2.down},
        {"West",Vector2.left},
        {"East",Vector2.right},
        {"NorthWest", new Vector2(-1,1)},
        {"SouthEast", new Vector2(1,-1)},
        {"NorthEast", Vector2.one},
        {"SouthWest", -Vector2.one}

    };
    //  public Dictionary<string, Vector2> ; 

    //event
    //public delegate void holdHandler(Vector2 a);
    //public static event holdHandler onHold;

    //public UnityEngine.Events.UnityAction<Vector2> onHold;
    //public event System.Action<Vector2> onHold; //hanya bisa dipanggil dari dalam kecuali subscribe

    public System.Action<bool, Vector2> onHold;
    public System.Action<int> onAddEnergy;
    public System.Action<int> onReduceEnergy;

    public System.Action onGameOver;
    public System.Action onFriendlyFire;
    public System.Action<bool> onAllowMove;

    private void Awake()
    {
        callEvent = this;

        _gameOver = false;

        waveInfo = new List<int>();


    }
    void Start()
    {   
        UI = ScUIManager9.Instance;
        Score = ScScore9.Instance;
        ScHighScoreManager.Load();//load
        Score._highscore = ScHighScoreManager.Highscore;//get
        UI.GamePlayUI.alpha = 0;
        restartStatic();

        SetWall();
        SpawnEvent(((byte)_theOthers.STAR), 20);//GakPenting suka2 aja.

        ScAudioManager.Play.PlayerSFX(AudioTag.PLAYER_SPAWN,1.4f);
        Invoke("DisableStarCollider", 1.5f);
        Invoke("StartofTheGame", 3f);

    }

    public void restartStatic()
    {  
        ScBullet.isPlaying = true; //ini berantakan harusnya isplaying di level manager.. //Todo perbaiki kapan2
        ScEnemy.isPlaying = true;
        ScEnergyFuel.isPlaying = true;
        ScHealthEnemy.enemyCount = EnemyStartCount;
        ScHealthEnemy.activeEnemy = EnemyStartCount;
        //!jangan sembarangan pake static..
        //*isplaying buat di gamemanager, enemyCo/active setiap new instance send di gamemanager aja, no static...
    }

    private void StartofTheGame()
    { ScAudioManager.Play.BackGroundMusic(true);
        currentEnemyWave = 0;
        UI.GamePlayUI.alpha = 1;
        AddEnergyEvent(100);
        SpawnEvent(((byte)_theOthers.ITEM), _theOthersCount);
        SpawnEvent(((byte)_theOthers.ENEMY), 1, 5f);
        //SpawnEvent(((byte)_theOthers.FXEXPLODESHIP),100,0.05f); //lucu 
    }

    void Update()
    {
        if (_lastRatio != _camera.aspect)
        {
            DeleteWall();
            SetWall();
        }
        
   
    }

    //Wall for Live
    //aku mau ubah wall dinamis untuk kebutuhan game
    void SetWall()
    {
        halfHeight = _camera.orthographicSize;
        halfWidth = _camera.aspect * halfHeight;

        _lastRatio = _camera.aspect;

        _min.Set(halfWidth, -halfHeight);
        _max.Set(-halfWidth, halfHeight);

        _walls[0] = Instantiate(_prefabWall, Vector3.up * halfHeight, Quaternion.identity);
        _walls[0].transform.localScale = Vector3.one / 2 + Vector3.left * (halfWidth * 2);
        _walls[1] = Instantiate(_walls[0], Vector3.down * halfHeight, Quaternion.identity);

        _walls[2] = Instantiate(_prefabWall, Vector3.right * halfWidth, Quaternion.identity);
        _walls[2].transform.localScale = Vector3.one / 2 + Vector3.up * (halfHeight * 2);
        _walls[3] = Instantiate(_walls[2], Vector3.left * halfWidth, Quaternion.identity);

        for (int i = 0; i < _walls.Length; i++)
        {
            _walls[i].transform.SetParent(transform);
        }
        //pake canvas aja mending lol..
    }

    void DeleteWall()
    {
        for (int i = 0; i < _walls.Length; i++)
        {
            Destroy(_walls[i]);
        }
        System.Array.Clear(_walls, 0, _walls.Length);
    }

    //Controll
    public void HoldEvent(bool hold, Vector2 pos)
    {
        onHold?.Invoke(hold, pos);
    }
    public void AddEnergyEvent(int point)
    {

        onAddEnergy?.Invoke(point);
        UI.SetCurrentHealth();
    }
    public void ReduceEnergyEvent(int point)
    {

        onReduceEnergy?.Invoke(point);
        UI.SetCurrentHealth();
    }

    public void AddScoreEvent(int point)
    {
        Score.AddScore(point);
        UI.SetScore();
    }
    public void AllowMoveEvent(bool Allow)
    {
        if (_player != null)
        {
            onAllowMove?.Invoke(Allow);

        }
    }
    
    public void EnemyRespawnEvent()
    {   //Lupa apa yang terjadi di bawah
        ScHealthEnemy.activeEnemy--;
        currentEnemyWave++;
        int totalEnemy;
        if (_playerHealth.Energy > 85)
        {
            if (currentEnemyWave > 3)
                totalEnemy = Random.Range(currentEnemyWave - 3, currentEnemyWave);
            else
            {
                totalEnemy = currentEnemyWave;
            }
            waveInfo.Add(totalEnemy);
        }
        else if (_playerHealth.Energy < 25)
        {
            totalEnemy = Random.Range(1, 2);
            if (currentEnemyWave > 10)
            {
                int total = 0;
                int totalInfo = 0;
                for (int n = waveInfo.Count - 8; n < waveInfo.Count - 1; n++)
                {
                    total += n - 1;
                    totalInfo = waveInfo[n];
                }
                if (totalInfo < total)
                {
                    totalEnemy = (int)(total - totalInfo * 0.75f);
                }
            }
            waveInfo.Add(totalEnemy);
        }
        else
        {
            totalEnemy = Random.Range(1, 3);
            waveInfo.Add(totalEnemy);
        }
        if (ScHealthEnemy.activeEnemy > 4)
        {   //TODO Kedepan sekaligus spawn Item yang Bagus,,,
            totalEnemy = Random.Range(0, 2);
            waveInfo.Add(totalEnemy);
        }
        SpawnEvent(((byte)_theOthers.ENEMY), totalEnemy, totalEnemy);

        int hpBonus = (int)(0.6f * (100+(ScHealthEnemy.activeEnemy*10) - _playerHealth.Energy));
        AddEnergyEvent(hpBonus);
    }
    //Spawn SpawnEvent buat spawn di public....
    //nanti tambah satu parameter untuk berbeda jenis paramater
    //pake dictionary mungkin nanti

    public Vector2 GetRandomPosition(float offset = 0.5f)
    {
        Vector2 pos;
        pos.x = Random.Range(_min.x - offset, _max.x + offset);
        pos.y = Random.Range(_min.y + offset, _max.y - offset);
        return pos;
    }

    public void GameOverEvent()
    {
        if (Score.Score > ScHighScoreManager.Highscore)
        {
            ScHighScoreManager.Highscore = Score.Score;//get
            Debug.Log("SCCCCCCCCORE" + Score.Score);
            ScHighScoreManager.Save();
        }

        _gameOver = true;
        onGameOver?.Invoke(); //lupa kalau bisa begini
                              // if (onGameOver != null)
                              // {}
    }

    public void FriendlyFireEvent()
    {
        onFriendlyFire?.Invoke();
    }


    //*Call THIS Nama, jumlah, durasi per spawn, x,y karna vector gak bisa default, gameobject
    public void SpawnEvent(byte DictKey, int n, float time = 0, float x = 69, float y = 69, GameObject gO = null)
    {//event without event lol pusing aku..
        StartCoroutine(SpawnOthers(DictKey, n, time, x, y, gO));
    }

    //!Don't Call This
    private IEnumerator SpawnOthers(byte DictKey, int count, float delayTime, float x, float y, GameObject gO = null)
    {
        for (int i = 0; i < count; i++)
        {
            if (delayTime > 0)
            {
                if (gO != null)
                {
                    if (!gO.gameObject.activeSelf)
                    {
                        break;
                    }
                }
                yield return new WaitForSeconds(delayTime);
            }

            if (_gameOver)
            {
                break;
            }

            if (gO != null)
            {
                if (!gO.activeSelf)
                {
                    break;
                }
                SpawnOther(DictKey, gO.transform.position.x, gO.transform.position.y);

            }
            else
                SpawnOther(DictKey, x, y);


            yield return new WaitUntil(() => { return !_isSpawning; });
        }
    }

    //!Don't call this
    public void SpawnOther(byte DictKey, float x, float y)//public karna button di luar itu ganggu
    {
        _isSpawning = true;
        bool skipCheck = false;
        Vector2 pos;
        if (x != 69)
        {
            pos = new Vector2(x, y);
            skipCheck = true;
        }
        else
        {
            pos = GetRandomPosition();
        }
        StartCoroutine(CheckPos(pos, skipCheck, (Vector2 newPos, bool isSpawn) =>
       {
           if (!isSpawn)
           {
               GameObject other = _AllOthers[DictKey].Find(o => !o.activeSelf);
               if (other != null)//notActive Found
               {
                   other.transform.position = newPos;
                   other.SetActive(true);
               }
               else
               {
                   _AllOthers[DictKey].Add(Instantiate(_theOthersPrefab[DictKey], newPos, Quaternion.identity));
               }//!Jadi selama ini aku lupa inisialisai objectnya  ke other... ahhhh sebel

           }
       }));
    }

    //*call this from anywhere
    public IEnumerator CheckPos(Vector2 pos, bool skip = false, System.Action<Vector2, bool> onCompleted = null)
    {
        bool found = false;
        if (!skip)
        {
            Collider2D hit;
            int i = 0;

            do
            {
                i++;
                Debug.Log(i);
                found = false;

                hit = Physics2D.OverlapCircle(pos, _spawnDistance, LayerMask.GetMask("Enemy"));
                //Debug.Log(hit.Length);
                Color col;
                switch (i)
                {
                    case 1: col = Color.red; break;
                    case 2: col = Color.yellow; break;
                    case 3: col = Color.green; break;
                    default: col = Color.white; break;
                }
                Debug.DrawLine(pos + _direction["South"].normalized * _spawnDistance, pos + _direction["North"].normalized * _spawnDistance, col, 5);
                Debug.DrawLine(pos + _direction["West"].normalized * _spawnDistance, pos + _direction["East"].normalized * _spawnDistance, col, 5);
                Debug.DrawLine(pos + _direction["NorthWest"].normalized * _spawnDistance, pos + _direction["SouthEast"].normalized * _spawnDistance, col, 5);
                Debug.DrawLine(pos + _direction["NorthEast"].normalized * _spawnDistance, pos + _direction["SouthWest"].normalized * _spawnDistance, col, 5);

                if (hit)
                {
                    pos = GetRandomPosition();
                    found = true;
                }

            } while (found && i != 10);

            yield return null;
        }
        onCompleted?.Invoke(pos, found);
        _isSpawning = false;
    }

    //? buat baru. soalnya aku lupa buat rotasi karna spawner di atas random point.. plus terlalu banyak paramater
    //!NOTE Kedepannya spawner di pisah antara jika perinta lebih dari satu, kayak random point itu...
    public GameObject PlayFX(byte DictKey, Vector2 pos, Quaternion rot)
    {
        GameObject other = _AllOthers[DictKey].Find(o => !o.activeSelf);
        if (other != null)//notActive Found
        {
            other.transform.position = pos;
            other.transform.rotation = rot;
            other.SetActive(true);
        }
        else
        {
            other = Instantiate(_theOthersPrefab[DictKey], pos, rot); //! JANGAN LUPA
            _AllOthers[DictKey].Add(other);
        }
        return other.gameObject;
    }

    private void DisableStarCollider()
    {
        foreach (GameObject star in _AllOthers[((byte)_theOthers.STAR)])
        {
            CircleCollider2D col = star.gameObject.GetComponent<CircleCollider2D>();
            col.enabled = !col.enabled;
        }
    }

    private void OnDisable()
    {

        CancelInvoke();
        StopAllCoroutines();
    }
    private void OnDestroy()
    {
        wasDestroyed = true;
    }

}
