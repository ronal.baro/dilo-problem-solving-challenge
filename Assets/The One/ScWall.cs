﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScWall : MonoBehaviour
{
    Vector3 scale;
    public float animationDuration;
    void Start()
    {
        Debug.Log(transform.localScale);
        scale = transform.localScale;
        transform.localScale = Vector3.zero;
        StartCoroutine(PlayAnimation());
    }

    private IEnumerator PlayAnimation()
    {   yield return new WaitForSeconds(1.5f);
        float count = 0;
        float distance = Vector3.Distance(scale, Vector3.zero);
        float velocity = distance / animationDuration;
        while (count <= animationDuration)
        {
            count += Time.deltaTime;
            transform.localScale = Vector3.Lerp(transform.localScale, scale, velocity * Time.deltaTime);
            yield return null;
        }
    }
}
